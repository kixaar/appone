import 'dart:async';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/locator/locator.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {

  @override
  _SplashScreenState createState() => _SplashScreenState();
}


class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    setupLocator();
    initializeFireBase();
    super.initState();

    Timer(
        Duration(seconds: 3),

            () => changeScreen());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.appBlue,
      body:Center(
        child: Container(
            height: 150,
            width: 150,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: MyColors.colorLogoOrange,
            ),
            child: Text(
              'LOGO',
              style: CTheme.textRegular25White(),
            ),
          ),
      ),
    );
  }

  void changeScreen()async{
    if(FirebaseAuth.instance.currentUser!=null)
      {
        Navigator.of(context)
            .pushNamedAndRemoveUntil('/profile_home', (Route<dynamic> route) => false);
      }
    else{
      Navigator.of(context)
          .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);
    }

  }

  void initializeFireBase() {
    Firebase.initializeApp();
  }
}