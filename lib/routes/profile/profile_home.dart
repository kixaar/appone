import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/my_bottom_bar.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/drawer/drawer_main.dart';
import 'package:app_one/utils/drawer/drawer_right.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';


class ProfileHome extends StatefulWidget {

  @override
  _ProfileHomeScreenState createState() => _ProfileHomeScreenState();
}


class _ProfileHomeScreenState extends State<ProfileHome> {
  var tabController = PageController();
  double widthOfProfileContent;
  double widthOfProfileContent2;
  int itemCount = 20;
  int tappedIndexRow2 = 0;
  int tappedIndexRow1 = 0;
  GlobalKey<ScaffoldState> scaffoldKey= GlobalKey();


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widthOfProfileContent = MediaQuery.of(context).size.width/4;
    widthOfProfileContent2 = MediaQuery.of(context).size.width/2.5;

    return Scaffold(

      key: scaffoldKey,
      drawer: DrawerMain(context: context,),
      endDrawer: DrawerRight(context: context,),
      backgroundColor: MyColors.colorDarkBlack,
      bottomNavigationBar: MyBottomNavigationBar(context:context,
      scaffoldKey: scaffoldKey,),
      body: BaseScrollView().baseView(context, [

        Padding(
          padding: EdgeInsets.only(top: 50,left:15,right: 15),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 65),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        height: 35,
                        decoration: BoxDecoration(
                          color:MyColors.searchBoxColor,
                          borderRadius: BorderRadius.circular(25)
                        ),
                        child: TextField(
                          onTap: ()=>{Navigator.pushNamed(context, '/profile_searched')},
                          style: CTheme.textRegular16White(),
                          decoration: InputDecoration(
                            hintText: 'Search',
                            border: UnderlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            prefixIcon: Icon(
                              Icons.search,
                              color: MyColors.colorWhite,
                              size: 20,
                            )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius: BorderRadius.circular(20)
                    ),
                    child: Icon(
                      Icons.add,
                      size: 35,
                      color: MyColors.colorWhite,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 30),
          child: SizedBox(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.only(left: 10,right: 10),
                    child: ListView.builder(
                        scrollDirection: Axis.horizontal,
                        itemCount: 20,
                        shrinkWrap: true,
                        itemBuilder: (context,position){
                          return itemStoryRow();
                        }),
                  ),
                ),
              ],
            ),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 55,left: 15,right: 15),
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  GestureDetector(
                    onTap: ()=>{
                    },
                    child: Text(
                      'Discover',
                      style: CTheme.textRegular25White(),
                    ),
                  )
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Text(
                    'Edit Categories',
                    style: CTheme.textRegular13White(),

                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10),
                    child: Container(
                      height: 20,
                      width: 20,
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                        color: MyColors.colorWhite,
                        borderRadius: BorderRadius.circular(20)
                      ),
                      child: Text(
                        '?',
                        style: CTheme.textRegular13Black(),
                      ),
                    ),
                  )
                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 30,left: 15,right: 15),
          child: SizedBox(
            height: 80,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 0,right: 0),
                      child: ListView.builder(
                          scrollDirection: Axis.horizontal,
                          itemCount: 10,
                          shrinkWrap: true,
                          itemBuilder: (context,position){
                            return itemCategoryRow(position);
                          }),
                    ),
                  ),
                ],
              ),

          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 0,left: 15,right: 15),
          child: SizedBox(
            height: 180,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 10,
                      shrinkWrap: true,
                      itemBuilder: (context,position){
                        return itemSquareStoryRow();
                      }),
                ),
              ],
            ),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 30,left: 15,right: 15),
          child: SizedBox(
            height: 80,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Expanded(
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: 5,
                      shrinkWrap: true,
                      itemBuilder: (context,position){
                        return itemCategoryRow2(position);
                      }),
                ),
              ],
            ),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 10),
          child: TabbarHeader(
            indicatorColor: MyColors.colorDarkBlack,
            backgroundColor: MyColors.colorDarkBlack,
            controller: tabController,
            tabs: [
              Tab(
                child: Text(
                  'All Posts',
                  style: CTheme.textRegular16White(),
                ),

              ),
              Tab(
                child: Text(
                  'Videos',
                  style: CTheme.textRegular16White(),
                ),
              ),
              Tab(
                child: Text(
                  'Images',
                  style: CTheme.textRegular16White(),
                ),
              ),
            ],
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 0),
          child: SizedBox(
            height:525.0 * 10,
            child: TabbarContent(
              controller: tabController,
              children: [

               Container(
                 child:Padding(
                   padding: const EdgeInsets.only(left: 15,right: 15),
                   child: Column(
                     mainAxisAlignment: MainAxisAlignment.start,
                     children: [
                       Row(
                         mainAxisAlignment: MainAxisAlignment.start,
                         crossAxisAlignment: CrossAxisAlignment.start,
                         children: [
                           Expanded(
                             child: Padding(
                               padding: const EdgeInsets.only(left: 25,right: 25),
                               child: Container(
                                 height: 35,
                                 decoration: BoxDecoration(
                                     color:MyColors.searchBoxColor,
                                     borderRadius: BorderRadius.circular(25)
                                 ),
                                 child: TextField(
                                   style: CTheme.textRegular16White(),
                                   decoration: InputDecoration(
                                       hintText: 'Search Hashtag',
                                       border: UnderlineInputBorder(
                                         borderSide: BorderSide.none,
                                       ),
                                       prefixIcon: Icon(
                                         Icons.search,
                                         color: MyColors.colorWhite,
                                         size: 20,
                                       )
                                   ),
                                 ),
                               ),
                             ),
                           ),
                         ],
                       ),

                       Padding(
                         padding: EdgeInsets.only(bottom: 20),
                         child:ListView.builder(
                             physics: NeverScrollableScrollPhysics(),
                             itemCount: 10,
                             shrinkWrap: true,
                             itemBuilder: (context,position){
                               return postItem();
                             }),
                       ),

                     ],
                   ),
                 ),
               ),

                Container(
                  child:Padding(
                    padding: const EdgeInsets.only(left: 15,right: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 25,right: 25),
                                child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color:MyColors.searchBoxColor,
                                      borderRadius: BorderRadius.circular(25)
                                  ),
                                  child: TextField(
                                    style: CTheme.textRegular16White(),
                                    decoration: InputDecoration(
                                        hintText: 'Search Hashtag',
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide.none,
                                        ),
                                        prefixIcon: Icon(
                                          Icons.search,
                                          color: MyColors.colorWhite,
                                          size: 20,
                                        )
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),

                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child:ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              shrinkWrap: true,
                              itemBuilder: (context,position){
                                return postItem();
                              }),
                        ),

                      ],
                    ),
                  ),
                ),

                Container(
                  child:Padding(
                    padding: const EdgeInsets.only(left: 15,right: 15),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 25,right: 25),
                                child: Container(
                                  height: 35,
                                  decoration: BoxDecoration(
                                      color:MyColors.searchBoxColor,
                                      borderRadius: BorderRadius.circular(25)
                                  ),
                                  child: TextField(
                                    style: CTheme.textRegular16White(),
                                    decoration: InputDecoration(
                                        hintText: 'Search Hashtag',
                                        border: UnderlineInputBorder(
                                          borderSide: BorderSide.none,
                                        ),
                                        prefixIcon: Icon(
                                          Icons.search,
                                          color: MyColors.colorWhite,
                                          size: 20,
                                        )
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),

                        Padding(
                          padding: EdgeInsets.only(bottom: 20),
                          child:ListView.builder(
                              physics: NeverScrollableScrollPhysics(),
                              itemCount: 10,
                              shrinkWrap: true,
                              itemBuilder: (context,position){
                                return postItem();
                              }),
                        ),

                      ],
                    ),
                  ),
                ),

              ],
            ),
          ),

        ),
      ]),
    );
  }

  Widget postItem() {
    return GestureDetector(
      onTap: ()=>{
        Navigator.pushNamed(context, '/comment_on_post')
      },
      child: Padding(
        padding: const EdgeInsets.only(top: 10, bottom: 10),
        child: Material(
          borderRadius: BorderRadius.circular(15),
          color: MyColors.appBlue,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Card(
                  elevation: 0,
                  color: MyColors.appBlue,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                                left: 5, top: 10, bottom: 10),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: MyColors.colorWhite,
                                  borderRadius: BorderRadius.circular(20)),
                              height: 40,
                              width: 40,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(left: 10),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'JohnDoe',
                                  style: CTheme.textRegular18White(),
                                  textAlign: TextAlign.start,
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 0),
                                  child: Text('2 Hours Ago',
                                      style: CTheme.textRegular11Grey()),
                                )
                              ],
                            ),
                          ),
                        ],
                      ),
                      Container(
                        height: 230,
                        child: Image.asset(
                          'assets/images/confirm_post/femaleimage.png',
                          fit: BoxFit.fill,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15, left: 15, right: 15),
                        child: Row(
                          children: [
                            Text(
                              Localization.stLocalized('hashTagText'),
                              style: CTheme.textRegular14LogoOrange(),
                              textAlign: TextAlign.start,
                            ),
                          ],
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 15, left: 15, right: 15),
                        child: SizedBox(
                          height: 65,
                          child: Row(
                            children: [
                              Expanded(
                                child: SingleChildScrollView(
                                  scrollDirection: Axis.vertical,
                                  child: Text(
                                    Localization.stLocalized('coventryIsACity'),
                                    style: CTheme.textRegular14White(),
                                    textAlign: TextAlign.start,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(
                            top: 30, left: 15, right: 15, bottom: 20),
                        child: Stack(
                          children: [
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Icon(
                                  Icons.favorite,
                                  color: MyColors.colorWhite,
                                  size: 20,
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '1125',
                                    style: CTheme.textRegular11White(),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 25),
                                  child: Icon(
                                    Icons.mode_comment,
                                    color: MyColors.colorWhite,
                                    size: 20,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '348',
                                    style: CTheme.textRegular11White(),
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 25),
                                  child: Icon(
                                    Icons.share,
                                    color: MyColors.colorWhite,
                                    size: 20,
                                  ),
                                ),
                                Padding(
                                  padding: EdgeInsets.only(left: 5),
                                  child: Text(
                                    '115',
                                    style: CTheme.textRegular11White(),
                                  ),
                                ),
                              ],
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Container(
                                  height: 20,
                                  width: 20,
                                  decoration: BoxDecoration(
                                      color: MyColors.colorWhite,
                                      borderRadius: BorderRadius.circular(30)),
                                  child: Icon(
                                    Icons.flag,
                                    color: MyColors.colorRedPlay,
                                    size: 15,
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegular11LogoOrange(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget roundedSquareButtonAddTopic(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 25,
              width: 150,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(5),
                  )
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 2),
                        child: Text(
                          btnText,
                          style: CTheme.textRegular11White(),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                     Padding(
                       padding: const EdgeInsets.only(right: 5),
                       child: Container(
                         height: 20,
                         width: 20,
                         decoration: BoxDecoration(
                           color: MyColors.appBlue,
                           borderRadius: BorderRadius.circular(20)
                         ),
                         child: Icon(
                           Icons.add,
                           color: MyColors.colorWhite,
                           size: 15,
                         ),
                       ),
                     )
                    ],
                  ),
                ],
              ),
            ),
          ),

      ],
    );
  }

  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }

  Widget itemStoryRow() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 5),
              child: SizedBox(
                height: 40,
                width: 40,
                child: DottedBorder(
                  dashPattern: [3],
                  borderType: BorderType.Circle,
                  color: MyColors.orangeStory,
                  strokeWidth: 2,
                  child: CircleAvatar(
                    radius: 50,
                    backgroundImage: AssetImage(
                        'assets/images/list_image/image_list_model.png'),
                  ),
                ),
              ),

          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: Text(
                'Thomas',
                style: CTheme.textRegular11White(),

              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget itemCategoryRow(int position) {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: GestureDetector(
                onTap: ()=>{
                  setState((){
                    tappedIndexRow1 = position;
                  })
                },
                child: Text(
                  'Gaming',
                  style:tappedIndexRow1 == position ?
                  CTheme.textRegular25White():CTheme.textRegular25blue(),
                  textAlign: TextAlign.start,

                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget itemCategoryRow2(int position) {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Column(
        children: [
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 10),
              child: GestureDetector(
                onTap: ()=>{
                  setState((){
                    tappedIndexRow2 = position;
                  })
                },
                child: Text(
                  'Trending',
                  style: tappedIndexRow2 == position ?
                  CTheme.textRegular25White():CTheme.textRegular25blue(),

                ),
              ),
            ),
          ),

        ],
      ),
    );
  }

  Widget itemSquareStoryRow() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
        child: Stack(
          children: [
               Padding(
                padding: const EdgeInsets.only(top: 10),
                child: Container(
                  width: 135,
                  decoration: BoxDecoration(
                      color: Colors.transparent,
                      borderRadius: BorderRadius.circular(5),
                    image: DecorationImage(
                      fit: BoxFit.fill,
                        image: AssetImage(
                            'assets/images/list_image/image_list_model.png'
                        ),
                    )
                  ),
                ),
              ),
            Padding(
              padding: EdgeInsets.only(left: 10,right: 10,bottom: 10),
              child:SizedBox(
                height: 180,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      height: 30,
                      width: 30,
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.circular(20),
                        border: Border.all(
                          color: MyColors.colorRedPlay,
                          width: 1
                        )
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(2),
                        child: Container(
                          height: 20,
                          width: 20,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(20),
                          ),
                          child: CircleAvatar(
                            radius: 20,
                            backgroundImage: AssetImage(
                                'assets/images/list_image/image_list_model.png'),
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 10,bottom: 10),
                      child: Text(
                        'Edgars',
                        style: CTheme.textRegular11White(),
                      ),
                    )
                  ],
                ),
              ),
            )
          ],
        ),

    );
  }

  Widget itemProfileContent() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
                  height: widthOfProfileContent,
                  width: widthOfProfileContent,
                  decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius:BorderRadius.circular(15)
                  ),
                  child: Image.asset(
                    'assets/images/list_image/image_list_model.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),



        ],
      ),
    );
  }

  Widget itemProfileContent2() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
                  height: widthOfProfileContent2,
                  width: widthOfProfileContent2,
                  decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius:BorderRadius.circular(15)
                  ),
                  child: Image.asset(
                    'assets/images/list_image/image_list_model.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),



        ],
      ),
    );
  }

}