import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/drawer/drawer_main.dart';
import 'package:app_one/utils/drawer/drawer_right.dart';
import 'package:app_one/utils/my_bottom_bar.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CommentPost extends StatefulWidget {

  @override
  _CommentPostScreenState createState() => _CommentPostScreenState();
}


class _CommentPostScreenState extends State<CommentPost> {

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      drawer: DrawerMain(context: context,),
      endDrawer: DrawerRight(context: context,),
        backgroundColor: MyColors.colorDarkBlack,
        bottomNavigationBar: MyBottomNavigationBar(context:context,
        scaffoldKey: scaffoldKey,),
        body: BaseScrollView().baseView(context, [

          Padding(
            padding: const EdgeInsets.only(top: 10,left: 15,right: 15),
            child: Material(
              borderRadius: BorderRadius.circular(15),
              color: MyColors.appBlue,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Card(
                      elevation: 0,
                      color: MyColors.appBlue,
                      child: Column(
                        children: [
                          Stack(
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Expanded(
                                    child: Container(
                                      height: 230,
                                      decoration: BoxDecoration(
                                        image: DecorationImage(
                                          fit: BoxFit.fill,
                                          image: AssetImage(
                                              'assets/images/confirm_post/femaleimage.png',
                                          )
                                        )
                                      ),
                                    ),
                                  ),
                                ],
                              ),

                              SizedBox(
                                height: 230,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(left: 30,bottom: 10),
                                      child: Container(
                                        decoration: BoxDecoration(
                                            color: MyColors.colorWhite,
                                            borderRadius: BorderRadius.circular(20)
                                        ),
                                        height: 40,
                                        width: 40,
                                      ),
                                    ),

                                    Padding(
                                      padding: EdgeInsets.only(left: 10,bottom: 10),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.end,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text('JohnDoe',style: CTheme.textRegular18White(),
                                            textAlign: TextAlign.start,),
                                          Padding(
                                            padding: const EdgeInsets.only(top: 0),
                                            child: Text('2 Hours Ago',style: CTheme.textRegular11Grey()),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ),

                            ],
                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                            child: Row(
                              children: [
                                Text(
                                  Localization.stLocalized('hashTagText'),
                                  style: CTheme.textRegular14LogoOrange(),
                                  textAlign: TextAlign.start,
                                ),
                              ],
                            ),
                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 15,left: 15,right: 15),
                            child: SizedBox(
                              child: Row(
                                children: [
                                  Expanded(
                                      child: Text(
                                        Localization.stLocalized('coventryIsACity'),
                                        style: CTheme.textRegular14White(),
                                        textAlign: TextAlign.start,
                                      ),

                                  ),

                                ],
                              ),
                            ),

                          ),

                          Padding(
                            padding:EdgeInsets.only(top: 30,left: 15,right: 15,bottom: 20),
                            child: Stack(
                              children: [
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: MyColors.colorWhite,
                                      size: 20,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '1125',
                                        style:CTheme.textRegular11White(),
                                      ),

                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 25),
                                      child: Icon(
                                        Icons.mode_comment,
                                        color: MyColors.colorWhite,
                                        size: 20,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '348',
                                        style:CTheme.textRegular11White(),
                                      ),
                                    ),
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.reply,
                                      color: MyColors.colorWhite,
                                      size: 20,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5),
                                      child: Text(
                                        '115',
                                        style:CTheme.textRegular11White(),
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ),
                          ),


                        ],
                      ),
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 0,bottom: 20),
                    child:ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: 10,
                        shrinkWrap: true,
                        itemBuilder: (context,position){
                          return Container(
                            decoration:
                            BoxDecoration(
                              gradient: LinearGradient(
                                  colors: [
                                    Color(0xFF242A37),
                                    Color(0xFF4E586E)
                                  ],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(1.0, 0.0),
                                  stops: [0.0, 1.0],
                                  tileMode: TileMode.clamp
                              ),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(top: 10),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.only(left: 5,bottom: 10),
                                        child: Container(
                                          decoration: BoxDecoration(
                                              color: MyColors.colorWhite,
                                              borderRadius: BorderRadius.circular(20)
                                          ),
                                          height: 20,
                                          width: 20,
                                        ),
                                      ),

                                      Padding(
                                        padding: EdgeInsets.only(left: 10,bottom: 10),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.end,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                              'JohnDoe',
                                              style: CTheme.textRegular16White(),
                                              textAlign: TextAlign.start,),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Expanded(
                                      child: Padding(
                                        padding: EdgeInsets.only(left: 15,bottom: 10),
                                        child:
                                        Text(
                                          'This is a comment and this is how a'
                                              ' comment looks like'
                                          ,style: CTheme.textRegular16White(),
                                          textAlign: TextAlign.start,),
                                      ),
                                    ),
                                  ],
                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Icon(
                                      Icons.reply,
                                      color: MyColors.colorWhite,
                                      size: 20,
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5,right: 15),
                                      child: Text(
                                        '12',
                                        style:CTheme.textRegular11White(),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(left: 5,right: 5),
                                      child: Text(
                                        'Comment',
                                        style:CTheme.textRegular16LogoOrange(),
                                      ),
                                    ),
                                  ],
                                ),

                                Padding(
                                  padding: const EdgeInsets.only(top:20),
                                  child: Container(
                                    height: 2,
                                    color: MyColors.colorWhite,
                                  ),
                                )
                              ],
                            ),
                          );
                        }),
                  ),


                ],
              ),
            ),
          ),

          Padding(
            padding: EdgeInsets.only(top: 30,bottom: 30,left: 50,right: 50),
            child: roundedSquareButton('ADD COMMENT',
                    ()=>{}
                    ),
          )

        ]),
    );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }

  Widget itemSearchProfile() {
    return Padding(
      padding: const EdgeInsets.only(top: 2),
      child: GestureDetector(
        onTap: ()=>{

        },
        child: Container(
          color: MyColors.appBlue,
          child: Stack(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left: 10,top: 10,bottom: 10),
                    child: Container(
                      decoration: BoxDecoration(
                          color: MyColors.colorWhite,
                          borderRadius: BorderRadius.circular(20)
                      ),
                      height: 40,
                      width: 40,
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(left: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('JohnDoe',style: CTheme.textRegular11White(),
                          textAlign: TextAlign.start,),
                        Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Text('Liked Your Post',style: CTheme.textRegular11White()),
                        )
                      ],
                    ),
                  ),
                ],
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Padding(
                    padding: EdgeInsets.only(right: 10,top: 5),
                    child: SizedBox(
                      height: 40,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(top: 0),
                            child: Text('View Profile',style: CTheme.textRegular11White(),
                              textAlign: TextAlign.start,),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}