import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/enums/viewstate.dart';
import 'package:app_one/utils/my_bottom_bar.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/drawer/drawer_main.dart';
import 'package:app_one/utils/drawer/drawer_right.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tabbar/tabbar.dart';


class ViewProfile extends StatefulWidget {

  @override
  _SocialProfileScreenState createState() => _SocialProfileScreenState();
}


class _SocialProfileScreenState extends State<ViewProfile> {
  var tabController = PageController();
  double widthOfProfileContent;
  double widthOfProfileContent2;
  int itemCount = 20;
  GlobalKey<ScaffoldState> scaffoldKey= GlobalKey();




  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    widthOfProfileContent = MediaQuery.of(context).size.width/4;
    widthOfProfileContent2 = MediaQuery.of(context).size.width/4;

    return Scaffold(
      key: scaffoldKey,
      drawer: DrawerMain(context: context,),
      endDrawer: DrawerRight(context: context,),
      backgroundColor: MyColors.colorDarkBlack,
      bottomNavigationBar: MyBottomNavigationBar(context:context,
      scaffoldKey: scaffoldKey,),
      body: BaseScrollView().baseView(context, [

        Padding(
          padding: EdgeInsets.only(top: 50,left:15,right: 15),
          child: Stack(
            children: [
              Padding(
                padding: const EdgeInsets.only(right: 65),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Expanded(
                      child: Container(
                        height: 35,
                        decoration: BoxDecoration(
                          color:MyColors.searchBoxColor,
                          borderRadius: BorderRadius.circular(25)
                        ),
                        child: TextField(
                          style: CTheme.textRegular16White(),
                          decoration: InputDecoration(
                            hintText: 'Search Chat',
                            border: UnderlineInputBorder(
                              borderSide: BorderSide.none,
                            ),
                            prefixIcon: Icon(
                              Icons.search,
                              color: MyColors.colorWhite,
                              size: 20,
                            )
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),

              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Container(
                    height: 35,
                    width: 35,
                    decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius: BorderRadius.circular(20)
                    ),
                    child: Icon(
                      Icons.add,
                      size: 35,
                      color: MyColors.colorWhite,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top: 20,left: 30),
          child:Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                height: 65,
                width: 65,
                decoration: BoxDecoration(
                  color: MyColors.colorWhite,
                  borderRadius: BorderRadius.circular(60)
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.only(left:30),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        Localization.stLocalized('antonio'),
                      style: CTheme.textRegular16White(),
                        textAlign: TextAlign.start,
                      ),
                      Text(
                        Localization.stLocalized('fashionModel'),
                      style: CTheme.textRegular11Grey(),
                        textAlign: TextAlign.start,

                      ),
                      Text(
                        Localization.stLocalized('antoniaCom'),
                      style: CTheme.textRegular11White(),
                        textAlign: TextAlign.start,

                      )
                    ],
                  ),
                ),
              )
            ],
          )
        ),

        Padding(
          padding: EdgeInsets.only(top:50,left: 50,right: 50),
         child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('489',
                    style: CTheme.textRegular16White(),
                    textAlign: TextAlign.center,

                  ),
                  Text(' Post',
                  style:CTheme.textRegular11LogoOrange(),
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(' 95.2k',
                    style: CTheme.textRegular16White(),
                    textAlign: TextAlign.center,
                  ),
                  Text('Followers',
                    style:CTheme.textRegular11LogoOrange(),
                  )
                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text('  763',
                    style: CTheme.textRegular16White(),
                    textAlign: TextAlign.center,

                  ),
                  Text('Following',
                    style:CTheme.textRegular11LogoOrange(),

                  )
                ],
              ),

            ],
          )
        ),

        Padding(
          padding: EdgeInsets.only(top: 10,left: 35,right: 35),
          child: Container(
            height: 1,
            color: MyColors.colorLogoOrange,
          ),
        ),
        
        Padding(
          padding: EdgeInsets.only(left: 35,top: 30,right: 35),
          child: roundedSquareButton('FOLLOWING', ()=>{
          }),
        ),

        Stack(
          children: [
            Visibility(
              visible: true,
              child: Container(
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 30),
                      child: Visibility(
                        visible: false,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Icon(
                              Icons.laptop,
                              color: MyColors.colorWhite,

                            ),
                            Padding(
                              padding: const EdgeInsets.only(left: 10),
                              child: Text(
                                'Website',
                                style: CTheme.textRegular16White(),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 25),
                      child: SizedBox(
                        height: 185,
                        child: Card(
                          color: MyColors.appBlue,
                          elevation: 5,
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(top:10),
                                child: roundedSquareButtonAddTopic('Add Topic', null),
                              ),
                              Expanded(
                                child:ListView.builder(
                                    scrollDirection: Axis.horizontal,
                                    itemCount: 10,
                                    shrinkWrap: true,
                                    itemBuilder: (context,position){
                                      return itemCategoryRow();
                                    }),

                              ),
                            ],
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 10),
                      child: TabbarHeader(
                        indicatorColor: MyColors.colorDarkBlack,
                        backgroundColor: MyColors.colorDarkBlack,
                        controller: tabController,
                        tabs: [
                          Tab(

                            child: Text(
                              'Gallery',
                              style: CTheme.textRegular16White(),
                            ),

                          ),
                          Tab(
                            child: Text(
                              'Videos',
                              style: CTheme.textRegular16White(),
                            ),
                          ),
                          Tab(
                            child: Text(
                              'Featured',
                              style: CTheme.textRegular16White(),
                            ),
                          ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: EdgeInsets.only(top: 0),
                      child: SizedBox(
                        height:widthOfProfileContent*itemCount/2,
                        child: TabbarContent(
                          controller: tabController,
                          children: [
                            Container(
                              color: MyColors.colorDarkBlack,
                              child:GridView.builder(
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 3),
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: itemCount,
                                  shrinkWrap: true,
                                  itemBuilder: (context, position) {
                                    return itemProfileContent();
                                  }),
                            ),

                            Container(
                              color: MyColors.colorDarkBlack,
                              child:GridView.builder(
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3),
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: itemCount,
                                  shrinkWrap: true,
                                  itemBuilder: (context, position) {
                                    return itemProfileContent();
                                  }),
                            ),
                            Container(
                              color: MyColors.colorDarkBlack,
                              child:GridView.builder(
                                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                      crossAxisCount: 3),
                                  scrollDirection: Axis.vertical,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: itemCount,
                                  shrinkWrap: true,
                                  itemBuilder: (context, position) {
                                    return itemProfileContent();
                                  }),
                            ),
                          ],
                        ),
                      ),

                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: false,
              child: Padding(
                padding: const EdgeInsets.only(top: 60),
                child: Container(
                  height: 165,
                  width: 165,
                  decoration: BoxDecoration(
                    gradient: LinearGradient(
                        colors: [Color(0xFF242A37), Color(0xFF4E586E)],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp),
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: Icon(
                    Icons.lock,
                    color: MyColors.colorWhite,
                    size: 85,
                  ),
                ),
              ),
            )
          ],
        ),
      ]),
    );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegular11LogoOrange(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget roundedSquareButtonAddTopic(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 25,
              width: 150,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(5),
                  )
              ),
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 2),
                        child: Text(
                          btnText,
                          style: CTheme.textRegular11White(),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                     Padding(
                       padding: const EdgeInsets.only(right: 5),
                       child: Container(
                         height: 20,
                         width: 20,
                         decoration: BoxDecoration(
                           color: MyColors.appBlue,
                           borderRadius: BorderRadius.circular(20)
                         ),
                         child: Icon(
                           Icons.add,
                           color: MyColors.colorWhite,
                           size: 15,
                         ),
                       ),
                     )
                    ],
                  ),
                ],
              ),
            ),
          ),

      ],
    );
  }

  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
      decoration: InputDecoration(
        enabledBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        border: UnderlineInputBorder(
          borderSide: BorderSide(color: MyColors.colorWhite),
        ),
        hintText:hintText,
        hintStyle: CTheme.textRegular18White(),

      ),
    );
  }

  Widget itemCategoryRow() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Column(
        children: [
          Text(
            'Gaming',
            style: CTheme.textRegular16White(),

          ),
          Padding(
            padding: const EdgeInsets.only(top: 5),
            child: Container(
              height: 100,
              width: 100,
              decoration: BoxDecoration(
                color: MyColors.appBlue,
                borderRadius:BorderRadius.circular(15)
              ),
              child: Image.asset(
                  'assets/images/list_image/image_list_model.png',
                fit: BoxFit.fill,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget itemProfileContent() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
                  height: widthOfProfileContent,
                  width: widthOfProfileContent,
                  decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius:BorderRadius.circular(15)
                  ),
                  child: Image.asset(
                    'assets/images/list_image/image_list_model.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),

        ],
      ),
    );
  }

  Widget itemProfileContent2() {
    return Padding(
      padding: const EdgeInsets.only(top:5,left: 5,right: 5),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(top: 5),
                child: Container(
                  height: widthOfProfileContent2,
                  width: widthOfProfileContent2,
                  decoration: BoxDecoration(
                      color: MyColors.appBlue,
                      borderRadius:BorderRadius.circular(15)
                  ),
                  child: Image.asset(
                    'assets/images/list_image/image_list_model.png',
                    fit: BoxFit.fill,
                  ),
                ),
              ),
            ],
          ),



        ],
      ),
    );
  }

}