import 'package:app_one/routes/about_us/about_us.dart';
import 'package:app_one/routes/chat_box/add_group_details.dart';
import 'package:app_one/routes/chat_box/chat_with_new_user.dart';
import 'package:app_one/routes/chat_box/notifications.dart';
import 'package:app_one/routes/chat_box/chat_main.dart';
import 'package:app_one/routes/chat_box/create_group.dart';
import 'package:app_one/routes/chat_box/group_chat.dart';
import 'package:app_one/routes/chat_box/oneToOne_chatbox.dart';
import 'package:app_one/routes/chat_box/social_inbox.dart';
import 'package:app_one/routes/create_account/create_account.dart';
import 'package:app_one/routes/create_account/create_buisness_profile.dart';
import 'package:app_one/routes/create_account/create_social_profile.dart';
import 'package:app_one/routes/create_account/select_profile_type.dart';
import 'package:app_one/routes/login/forgot_password.dart';
import 'package:app_one/routes/login/login.dart';
import 'package:app_one/routes/profile/comment_post.dart';
import 'package:app_one/routes/profile/profile_searched.dart';
import 'package:app_one/routes/profile/view_profile.dart';
import 'package:app_one/routes/profile/profile_home.dart';
import 'package:app_one/routes/settings/group_settings.dart';
import 'package:app_one/routes/settings/message_options.dart';
import 'package:app_one/routes/settings/settings.dart';
import 'package:app_one/routes/settings/single_chat_settings.dart';
import 'package:app_one/routes/splash/splash_screen.dart';
import 'package:app_one/routes/upload_Image_video/upload_image.dart';
import 'package:app_one/routes/upload_Image_video/upload_video.dart';
import 'package:flutter/material.dart';

class Routers{
  static Route<dynamic> generateRoute(RouteSettings settings)
  {
    switch (settings.name)
    {
      case '/':
        return MaterialPageRoute(builder: (_)=> SplashScreen());
      case '/login':
        return MaterialPageRoute(builder: (_)=> Login());
      case '/forgot_password':
        return MaterialPageRoute(builder: (_)=> ForgotPassword());
      case '/create_account':
        return MaterialPageRoute(builder: (_)=> CreateAccount());
      case '/select_profile_type':
        return MaterialPageRoute(builder: (_)=> SelectProfileType());
      case '/create_social_profile':
        return MaterialPageRoute(builder: (_)=> CreateSocialProfile());
      case '/create_business_profile':
        return MaterialPageRoute(builder: (_)=> CreateBusinessProfile());
      case '/notifications':
        return MaterialPageRoute(builder: (_)=> NotificationsScreen());
      case '/chat_main':
        return MaterialPageRoute(builder: (_)=> ChatMain());
      case '/about_us':
        return MaterialPageRoute(builder: (_)=> AboutUs());
      case '/settings':
        return MaterialPageRoute(builder: (_)=> Settings());
      case '/upload_image':
        return MaterialPageRoute(builder: (_)=> UploadImage());
      case '/upload_video':
      return MaterialPageRoute(builder: (_)=> UploadVideo());
      case '/view_profile':
        return MaterialPageRoute(builder: (_)=> ViewProfile());
      case '/profile_home':
        return MaterialPageRoute(builder: (_)=> ProfileHome());
      case '/profile_searched':
      return MaterialPageRoute(builder: (_)=> ProfileSearched());
      case '/comment_on_post':
        return MaterialPageRoute(builder: (_)=> CommentPost());
      case '/social_inbox':
        return MaterialPageRoute(builder: (_)=> SocialInbox());
      case '/create_group':
        return MaterialPageRoute(builder: (_)=> CreateGroup());
      case '/chat_with_new_user':
        return MaterialPageRoute(builder: (_)=> ChatWithNewUser());
      case '/group_details':
        return MaterialPageRoute(builder: (_)=> AddGroupDetails(
          selectedGroupUsers: settings.arguments,));
      case '/group_chat':
        return MaterialPageRoute(builder: (_)=> GroupChat(
          groupData: settings.arguments,));
      case '/group_settings':
        return MaterialPageRoute(builder: (_)=> GroupSettings(groupData: settings.arguments,));
      case '/one_to_one_chatbox':
        return MaterialPageRoute(builder: (_)=> OneToOneChatBox(
          chatWithUserDetails:settings.arguments,));
      case '/message_options':
        return MaterialPageRoute(builder: (_)=> MessageOptions());
      case '/single_chat_settings':
        return MaterialPageRoute(builder: (_)=> SingleChatSettings(
          selectedUserId: settings.arguments,
        ));
      default:
        return MaterialPageRoute(builder: (_)=> Scaffold(
          body: Center(child: Text('No Route Defined For ${settings.name}'),),
        ));
    }
  }
}