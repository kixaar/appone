
import 'dart:async';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CreateAccount extends StatefulWidget {

  @override
  _CreateAccountScreenState createState() => _CreateAccountScreenState();
}


class _CreateAccountScreenState extends State<CreateAccount> {
  var isTermConditionsChecked = false;

  String _valueGender;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: MyColors.appBlue,
      body: BaseScrollView().baseView(context, [
        Padding(
          padding: EdgeInsets.only(top:50,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('firstName')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('lastName')),
        ),

        Padding(
          padding: const EdgeInsets.only(top: 30,left: 40,right: 40),
          child: Container(
            height: 50,
            child: Padding(
              padding: const EdgeInsets.fromLTRB(0, 0, 0, 0),
              child: Theme(
                data: Theme.of(context).copyWith(
                    canvasColor: MyColors.appBlue),
                child: ButtonTheme(
                  child: DropdownButton<String>(
                    underline: Container(
                      color: MyColors.colorWhite,
                      height: 1,
                    ),
                    iconEnabledColor: Colors.white,
                    iconDisabledColor: Colors.white,
                    hint:Text(
                        'Gender',
                        style: CTheme.textRegular18White(),
                        textAlign: TextAlign.start,
                      ),

                    isExpanded: true,
                    items: dropDownMenuItems(),
                    onChanged: (value) {
                      setState(() {
                        _valueGender = value;
                      });
                    },
                    value: _valueGender,
                  ),
                ),
              ),
            ),
          ),
        ),

        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('dateOfBirth')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('emailAddress')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('country')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('city')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('createPassword')),
        ),
        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('confirmPassword')),
        ),

        Padding(
          padding: EdgeInsets.only(top:30,left: 40,right: 30),
          child: acceptAgreementRow(),
        ),
        Padding(
          padding: EdgeInsets.only(top: 30,left: 40,right: 40,bottom: 35),
          child: roundedSquareButton(
              Localization.stLocalized('createAccount'),
                  ()=>{
                Navigator.pushNamed(context, '/select_profile_type')
              }
          ),
        ),

      ])
    );
  }

  Stack acceptAgreementRow() {
    return Stack(
          children: [
            SizedBox(
              height: 35,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    Localization.stLocalized('iAccept'),
                    style: CTheme.textRegular14White(),
                  ),
                  Text(
                    Localization.stLocalized('termAndCondition'),
                    style: CTheme.textRegular14Blue(),
                  ),
                ],
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Theme(
                  data:ThemeData(
                    unselectedWidgetColor: MyColors.colorWhite,
                  ),
                  child: SizedBox(
                    height: 35,
                    width: 35,
                    child: Checkbox(
                      activeColor: Colors.white,
                        value: isTermConditionsChecked,
                        onChanged: (value) {
                          termsConditionsCb(value);
                        },
                        checkColor: Colors.black, // color of tick Mark
                      ),
                  ),
                ),

              ],
            )
          ],
        );
  }


  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            hintText:hintText,
            hintStyle: CTheme.textRegular18White(),

          ),
        );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
              decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
              ),
              child: Text(
                btnText,
                style: CTheme.textRegularBold18White(),
              ),
            ),
          ),
        ),
      ],
    );
  }

  List<DropdownMenuItem<String>> dropDownMenuItems() {
    return [
      DropdownMenuItem(
        value: "1",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Text(
              "",
              style: CTheme.textRegular18White(),
            ),
            Expanded(
              child: Text(
                "Male",
                style: CTheme.textRegular18White(),
              ),
            ),
          ],
        ),
      ),
      DropdownMenuItem(
        value: "2",
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              height: 20,
              child: Text(
                "",
                style: CTheme.textRegular18White(),
              ),
            ),
            Expanded(
              child: Text(
                "Female",
                style: CTheme.textRegular18White(),
              ),
            ),
          ],
        ),
      ),
    ];
  }

  void termsConditionsCb(bool value) {
    if (isTermConditionsChecked == false) {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isTermConditionsChecked = true;
      });
    } else {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isTermConditionsChecked = false;
      });
    }
  }

}