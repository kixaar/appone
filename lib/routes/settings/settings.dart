
import 'dart:async';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/drawer/drawer_main.dart';
import 'package:app_one/utils/drawer/drawer_right.dart';
import 'package:app_one/utils/my_bottom_bar.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {

  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}


class _SettingsScreenState extends State<Settings> {

  GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
        drawer: DrawerMain(context: context,),
        endDrawer: DrawerRight(context: context,),
      bottomNavigationBar: MyBottomNavigationBar(context: context,
      scaffoldKey: scaffoldKey,),
      backgroundColor: MyColors.colorDarkBlack,
      body: BaseScrollView().baseView(context, [
        
        Padding(
          padding: EdgeInsets.only(top: 60),
          child: dropDownVideoItem('Notification Settings',
                  ()=>{}),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: dropDownVideoItem('Blocked Users',
                  ()=>{}),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: dropDownVideoItem('Terms And Conditions',
                  ()=>{}),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: dropDownVideoItem('Privacy Policy',
                  ()=>{}),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: dropDownVideoItem('Chat Settings',
                  ()=>{}),
        ),
        Padding(
          padding: EdgeInsets.only(top: 0),
          child: dropDownVideoItem('Unsubscribe',
                  ()=>{}),
        ),

      ])
    );
  }


  Widget dropDownVideoItem(String text,Function onTap) {
    return
      SizedBox(
        height: 60,
        child: GestureDetector(
          onTap: onTap,
          child: Card(
            elevation: 3,
            color: MyColors.appBlue,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 20),
                  child: Text(
                    text,
                    style: CTheme.textRegular25White(),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 10),
                  child: Icon(
                    Icons.play_arrow,
                    size: 30,
                    color: MyColors.colorWhite,
                  ),
                )
              ],
            ),
          ),
        ),
      );

  }



}