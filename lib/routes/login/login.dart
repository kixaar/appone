
import 'dart:async';
import 'dart:io';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/globals/globals.dart';
import 'package:app_one/locator/locator.dart';
import 'package:app_one/model/response/login__response.dart';
import 'package:app_one/routes/baseView/base_view.dart';
import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:app_one/utils/image_picker/image_picker.dart';
import 'package:app_one/viewmodel/login_vmodel.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class Login extends StatefulWidget {

  @override
  _LoginScreenState createState() => _LoginScreenState();
}


class _LoginScreenState extends State<Login> {
  TextEditingController emailTextController = TextEditingController();
  TextEditingController passTextController = TextEditingController();
  PickedFile imageFile;

  String abc;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BaseView<LoginViewModel>(
      onModelReady: (model) async {

      },
      builder: (context, model, child) => loginView(context, model),
    );
  }

  Scaffold loginView(BuildContext context,LoginViewModel model) {
    return Scaffold(
    backgroundColor: MyColors.appBlue,
    body: BaseScrollView().baseView(context, [
      //Round Logo
      Padding(
        padding: EdgeInsets.only(top: 100),
        child:imageFile==null?
        Container(
          height: 150,
          width: 150,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(100),
            color: MyColors.colorLogoOrange,
          ),
          child: IconButton(
            icon: Icon(
              Icons.add,
              size: 35,
              color: MyColors.colorWhite,
            ),
            onPressed: ()async{
             imageFile = await PickImageController.instance.picker
                 .getImage(source: ImageSource.gallery);
             if(imageFile!=null)
               {
                 setState(() {
                 });
               }
            },
          )
        )
        :Container(
          height: 150,
            width: 150,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(75)),
                child: Image.file(File(imageFile.path)
                ,fit: BoxFit.cover,))),
      ),

      Padding(
        padding: EdgeInsets.only(top:50,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('email'),
            emailTextController),
      ),

      Padding(
        padding: EdgeInsets.only(top:30,left: 40,right: 40),
        child: plainTextField(Localization.stLocalized('password'),
        passTextController),
      ),

      Padding(
        padding: EdgeInsets.only(top: 50,left: 40,right: 40),
        child: roundedSquareButton(Localization.stLocalized('login'),
            ()async{
          if(emailTextController.text.isNotEmpty&&passTextController.text.isNotEmpty)
              {
                String loginResult =
                    await model.login(emailTextController.text, passTextController.text);
                if(loginResult == 'Successful')
                  {
                    var tokenResult= await model.updateUserFcmToken();
                    if(tokenResult)
                      {
                        Navigator.pushNamedAndRemoveUntil(
                            context, '/profile_home', (route) => false);
                      }
                    else{
                      FirebaseAuth.instance.signOut();
                      CTheme.showAppAlertOneButton(
                          context: context,
                          title: 'Error',
                          bodyText: "Error Logging In",
                          handler2: (action) => {
                            Navigator.pop(context),
                          });
                    }

                  }
                    else{
                  CTheme.showAppAlertOneButton(
                      context: context,
                      title: 'Error',
                      bodyText: loginResult,
                      handler2: (action) => {
                        Navigator.pop(context),
                      });
                }
              }

          else{
            CTheme.showAppAlertOneButton(context: context,title: 'Error',
                bodyText:'Fields Should Not Be Empty' );
          }
            }),
      ),

      Padding(
        padding: EdgeInsets.only(top: 30,left: 40,right: 40),
        child: roundedSquareButton(
            Localization.stLocalized('createAccount'),
            ()async{
              if(emailTextController.text.isNotEmpty&&
              passTextController.text.isNotEmpty&& imageFile != null)
              {
                        String signUpResult=await model.createUser(
                            email:emailTextController.text,
                            password:passTextController.text,
                            userImageFile:File(imageFile.path));
                        signUpResult=='Successful'
                        ?Navigator.pushNamed(context, '/profile_home')
              :CTheme.showAppAlertOneButton(context: context,title: 'Error',
                        bodyText: signUpResult);
                      }
              else{
                CTheme.showAppAlertOneButton(context: context,title: "Error",
                bodyText: "Image, Name and Password Should'nt Be Empty");
              }
                      // Navigator.pushNamed(context, '/create_account')
              // model.createUserWithEP()

            }
        ),
      ),

      Padding(
        padding: EdgeInsets.only(top: 20,bottom: 30),
        child: GestureDetector(
          onTap: ()=>{
            Navigator.pushNamed(context, '/forgot_password')
          },
          child: Text(
              Localization.stLocalized('forgotPassword'),
            style: CTheme.textRegularBoldItalic14White(),
          ),
        ),
      )

    ])
  );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
                  decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
                  ),
                  child: Text(
                    btnText,
                    style: CTheme.textRegularBold18White(),
                  ),
                ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText,TextEditingController controller) {
    return TextField(
      controller: controller,
      style: CTheme.textRegular18White(),
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            hintText:hintText,
            hintStyle: CTheme.textRegular18White(),

          ),
        );
  }

}