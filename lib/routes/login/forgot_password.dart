
import 'dart:async';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/localization.dart';
import 'package:app_one/utils/base_view/base_view.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ForgotPassword extends StatefulWidget {

  @override
  _ForgotPasswordScreenState createState() => _ForgotPasswordScreenState();
}


class _ForgotPasswordScreenState extends State<ForgotPassword> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.appBlue,
      body: BaseScrollView().baseView(context, [
        Padding(
          padding: EdgeInsets.only(top: 100),
          child:Container(
            height: 150,
            width: 150,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100),
              color: MyColors.colorLogoOrange,
            ),
            child: Text(
              Localization.stLocalized('logo'),
              style: CTheme.textRegular25White(),
            ),
          ),
        ),
        Padding(
          padding: EdgeInsets.only(top:50,left: 40,right: 40),
          child: plainTextField(Localization.stLocalized('emailAddress')),
        ),

        Padding(
          padding: EdgeInsets.only(top: 50,left: 40,right: 40),
          child: roundedSquareButton(Localization.stLocalized('done'),
              null),
        ),
      ])
    );
  }

  Widget roundedSquareButton(String btnText,Function onTap) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              alignment: Alignment.center,
              height: 50,
                  decoration: (
                  BoxDecoration(
                    gradient: LinearGradient(
                        colors: [
                          Color(0xFF242A37),
                          Color(0xFF4E586E)
                        ],
                        begin: const FractionalOffset(0.0, 0.0),
                        end: const FractionalOffset(1.0, 0.0),
                        stops: [0.0, 1.0],
                        tileMode: TileMode.clamp
                    ),
                    borderRadius: BorderRadius.circular(10),
                  )
                  ),
                  child: Text(
                    btnText,
                    style: CTheme.textRegularBold18White(),
                  ),
                ),
          ),
        ),
      ],
    );
  }

  TextField plainTextField(String hintText) {
    return TextField(
      style: CTheme.textRegular18White(),
          decoration: InputDecoration(
            enabledBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            focusedBorder: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            border: UnderlineInputBorder(
              borderSide: BorderSide(color: MyColors.colorWhite),
            ),
            hintText:hintText,
            hintStyle: CTheme.textRegular18White(),

          ),
        );
  }

}