import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class BaseScrollView{

  Widget baseView(BuildContext context,List<Widget> children){
    return SingleChildScrollView(
      scrollDirection: Axis.vertical,
     child: Center(
          child: Column(
              children: children
          ),
        ),
    );
  }
}