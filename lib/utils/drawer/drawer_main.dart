import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/MyColors.dart';
import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/viewmodel/login_vmodel.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerMain extends StatefulWidget{

  final BuildContext context;

  DrawerMain({this.context});

  @override
  DrawerMainState createState() => DrawerMainState();
}

class DrawerMainState extends State<DrawerMain>{



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Theme(
        data: Theme.of(context).copyWith(
          canvasColor: MyColors.colorDarkBlack
        ),
        child: Drawer(
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              color: MyColors.colorDarkBlack,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(top: 35,left: 40),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Container(
                          height: 70,
                          width: 70,
                          child: CircleAvatar(
                            backgroundColor: MyColors.colorWhite,
                            child: Icon(
                              Icons.person,
                              size: 51,
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20),
                          child: Column(
                            children: [
                              Text(
                                'John Doe',
                                style: CTheme.textRegular21White(),
                              ),
                              Text(
                                'Social Account',
                                style: CTheme.textRegular11WhiteItalic(),
                              )
                            ],
                          ),
                        )
                      ],
                    ),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 50, left: 30),
                    child:
                    navTextItem( 'Home',
                        ()=>{
                      Navigator.pushNamed(context, '/profile_home')
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child: navTextItem('Inbox',
                            ()=>{
                      Navigator.pushNamed(context, "/social_inbox")
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child: navTextItem( 'Notifications',
                            ()=>{
                      Navigator.pushNamed(context, '/notifications')
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child:
                    navTextItem( 'Create New Post',
                            ()=>{
                      Navigator.pop(context),
                      Navigator.pushNamed(context, '/upload_image')
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child: navTextItem('Settings',
                            ()=>{
                      Navigator.pushNamed(context, '/settings')
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child:
                    navTextItem('About Us',
                            ()=>{
                      Navigator.pushNamed(context, '/about_us')
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 20, left: 30),
                    child: navIconTextIcon(
                        "Advertise With Us",
                            ()=>{
                        }),
                  ),

                  Padding(
                    padding: EdgeInsets.only(top: 100, left: 30,bottom: 30),
                    child: navIconTextItemNoLine( "Logout",
                            ()=>{
                              FirebaseFirestore.instance.collection("users")
                                  .doc(FireBaseService.getCurrentUserUid()).update({
                                'fcmToken':null
                              }).then((value) => {
                                FirebaseAuth.instance.signOut(),
                                print("Login: Token Update Successful "),
                                Navigator.pushNamed(context, '/login')
                              }).catchError((error)=>{
                                print(error),
                                CTheme.showAppAlertOneButton(
                                    context: context,
                                    title: 'Error',
                                    bodyText: "Error Logging Out!",
                                    handler2: (action) => {
                                      Navigator.pop(context),
                                    })
                              }),

                        }),
                  ),



                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget navTextItem( String text,Function onTap) {
    return GestureDetector(
      onTap: onTap,
        child: Row(
          children: [
            Expanded(
              flex: 2,
              child: Container(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                        Row(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Container(
                              decoration: BoxDecoration(
                                border: Border(
                                  bottom: BorderSide(
                                    color: MyColors.colorWhite,
                                    width: 1
                                  )
                                )
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(left: 5,right: 5,top: 5,bottom: 5),
                                child: Text(
                                  text,
                                  style: CTheme.textRegular18White(),
                                ),
                              ),
                            ),
                          ],
                        ),

                  ],
                ),
              ),
            ),
          ],
        ),

    );
  }

  Widget navIconTextItemNoLine( String text,Function onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisSize: MainAxisSize.min,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [

                      Padding(
                          padding: const EdgeInsets.only(left: 5,right: 5,top: 5
                              ,bottom: 5),
                          child: Text(
                            text,
                            style: CTheme.textRegular18White(),
                          ),
                        ),

                    ],
                  ),

                ],
              ),
            ),
          ),
        ],
      ),

    );
  }

  Widget navIconTextIcon( String text,Function onTap) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 5,right: 5,top: 5
                            ,bottom: 5),
                        child: Text(
                          text,
                          style: CTheme.textRegular18White(),
                        ),
                      ),

                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 10,top: 5),
                        child: Container(
                          height: 20,
                          width: 20,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: MyColors.colorWhite,
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child: Text(
                            '?',
                            style: CTheme.textRegular13Black(),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),

    );
  }
}
