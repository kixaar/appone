import 'package:app_one/constants/CTheme.dart';
import 'package:app_one/constants/MyColors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DrawerRight extends StatefulWidget{

  final BuildContext context;

  DrawerRight({this.context});

  @override
  DrawerRightState createState() => DrawerRightState();
}

class DrawerRightState extends State<DrawerRight>{



  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Drawer(
        child: SingleChildScrollView(
          scrollDirection: Axis.vertical,
          child: Container(
            height: MediaQuery.of(context).size.height,
            color: MyColors.colorDarkBlack,
            child: Column(
              children: [
                Padding(
                  padding: const EdgeInsets.only(top: 35,left: 40),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: 70,
                        width: 70,
                        child: CircleAvatar(
                          backgroundColor: MyColors.colorWhite,
                          child: Icon(
                            Icons.person,
                            size: 51,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20),
                        child: Column(
                          children: [
                            Text(
                              'John Doe',
                              style: CTheme.textRegular21White(),
                            ),
                            Text(
                              'Social Account',
                              style: CTheme.textRegular11WhiteItalic(),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ),

                Padding(
                  padding: EdgeInsets.only(top: 50, left: 20),
                  child:
                  navRightTextIcon( 'Social Account',
                      ()=>{
                        Navigator.pushNamed(context, '/view_profile'),                      },
                      true),
                ),

                Padding(
                  padding: EdgeInsets.only(top: 20, left: 20),
                  child: navRightTextIcon('My Business',
                          ()=>{
                      },
                      false),
                ),


              ],
            ),
          ),
        ),
      ),
    );
  }


  Widget navRightTextIcon( String text,Function onTap,bool visibility) {
    return GestureDetector(
      onTap: onTap,
      child: Row(
        children: [
          Expanded(
            flex: 2,
            child: Container(
              child: Stack(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Visibility(
                        visible: visibility,
                        child: Padding(
                          padding: const EdgeInsets.only(right: 5,top: 8
                              ),
                          child: Container(
                            decoration: BoxDecoration(
                                color: MyColors.colorLogoOrange,
                                borderRadius: BorderRadius.circular(15)
                            ),
                            height: 15,
                            width: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left: 20,right: 8,top: 5
                            ,bottom: 5),
                        child: Text(
                          text,
                          style: CTheme.textRegular18White(),
                        ),
                      ),

                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 15,top: 5),
                        child: Container(
                          height: 20,
                          width: 20,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: MyColors.colorRedPlay,
                              borderRadius: BorderRadius.circular(20)
                          ),
                          child: Text(
                            '1',
                            style: CTheme.textRegular11White(),
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
        ],
      ),

    );
  }
}
