import 'package:app_one/locator/locator.dart';
import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/viewmodel/base_model.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginViewModel extends BaseModel {

  FireBaseService fireBaseService = locator<FireBaseService>();

  Future<String> login(email,password)async{
    return await fireBaseService.loginUserWithEmailPassword(email: email,
        password: password);
  }

  Future<String> createUser({email,password,userImageFile}) async {
    return await fireBaseService.createUserWithEmailPass(email: email,password: password,
        userImageFile: userImageFile);
  }

  Future<bool> updateUserFcmToken()async{
    SharedPreferences pref = await SharedPreferences.getInstance();
    var update=false;
    var tokenResult =await fireBaseService.getToken();
    if(tokenResult != 'Failed')
      {
        await FirebaseFirestore.instance.collection("users")
            .doc(FireBaseService.getCurrentUserUid()).update({
          'fcmToken':tokenResult
        }).then((value) => {
          pref.setString('userFcmToken', tokenResult),
         print("Login: Token Update Successful $tokenResult"),
         update = true
        }).catchError((error)=>{
          print(error),
          update = false
        });
      }
    else{
      update = false;
    }
   return update;
  }
}