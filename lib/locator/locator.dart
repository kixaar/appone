import 'package:app_one/services/firebase/firebase_service.dart';
import 'package:app_one/viewmodel/add_group_detail_vmodel.dart';
import 'package:app_one/viewmodel/login_vmodel.dart';
import 'package:app_one/viewmodel/onoToOne_vmodel.dart';
import 'package:get_it/get_it.dart';


GetIt locator = GetIt.instance;

void setupLocator() {

  locator.registerFactory(() => FireBaseService());
  locator.registerFactory(() => LoginViewModel());
  locator.registerFactory(() => OneToOneViewModel());
  locator.registerFactory(() => AddGroupDetailViewModel());

}
